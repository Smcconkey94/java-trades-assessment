package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.citi.training.trades.model.Trade;

public class TradeTests {

	private int testId = 53;
    private String testStock = "AAPL";
    private double testPrice = 150;
    private int testVolume = 1;
    
    @Test
    public void test_Trade_constructor() {
        Trade testTrade = new Trade(testId, testPrice, testStock, testVolume);;

        assertEquals(testId, testTrade.getId());
        assertEquals(testStock, testTrade.getStock());
        assertEquals(testPrice, testTrade.getPrice(), 0.0001);
        assertEquals(testVolume, testTrade.getVolume());

    }

    @Test
    public void test_Trade_toString() {
        String testTradeString = new Trade(testId, testPrice, testStock, testVolume).toString();

        assertTrue(testTradeString.contains((new Integer(testId)).toString()));
        assertTrue(testTradeString.contains(testStock));
        assertTrue(testTradeString.contains(String.valueOf(testPrice)));
        assertTrue(testTradeString.contains((new Integer(testVolume)).toString()));

    }
}