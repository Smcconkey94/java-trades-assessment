package com.citi.training.trades.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {
	
	@Autowired
	MysqlTradeDao mysqlTradeDao;
	
	@Test
    @Transactional
    public void test_createAndFindAll() {
		mysqlTradeDao.create(new Trade(-1, 10.0, "APPL", 10));

        assertEquals(1, mysqlTradeDao.findAll().size());
    }
	
	@Test
	@Transactional
	public void test_createAndFindByID() {
		mysqlTradeDao.create(new Trade(-1, 999.99, "AAPL", 2));
		assertEquals(mysqlTradeDao.findById(4).getId(), 4);
		assertEquals(mysqlTradeDao.findById(4).getStock(), "AAPL");
		assertEquals(mysqlTradeDao.findById(4).getPrice(), 999.99,  0.001);
		assertEquals(mysqlTradeDao.findById(4).getVolume(), 2);

	}
	
	@Test(expected = TradeNotFoundException.class)
	@Transactional
	public void test_FindByIDReturnsEmployeeNotFoundException() {
		mysqlTradeDao.findById(999);
	}
	
	@Test
	@Transactional
	public void test_DeleteByIdDeletesTrade() {
		mysqlTradeDao.create(new Trade(-1, 999.99, "AAPL", 2));
		mysqlTradeDao.create(new Trade(-1, 123.99, "MSFT", 10));
		assertEquals(mysqlTradeDao.findAll().size(),2);
		mysqlTradeDao.deleteById(2);
		assertEquals(mysqlTradeDao.findAll().size(),1);
	}
	
	@Test(expected = TradeNotFoundException.class)
	@Transactional
	public void test_DeleteEmployeeThrowsEmployeeNotFoundExeptionWhenIdDoesntExist() {
		mysqlTradeDao.deleteById(9999999);
	}
	
}
