package com.citi.training.trades.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradesService;

/**
 * REST Interface class for {@link com.citi.training.trades.model.Trade} domain object.
 * @author Stephen McConkey
 * @see Trade
 */

@RestController
@RequestMapping("/trades")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
 
    @Autowired
    private TradesService tradesService;
    
    /**
     * Find a list of all {@link com.citi.training.trades.model.Trade}.
     * @return a List of all {@link com.citi.training.trades.model.Trade} that were found or HTTP 404
     */
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> findAll(){
        LOG.debug("findAll() was called");
        return tradesService.findAll();
    }
    
    /**
     * Find an {@link com.citi.training.trades.model.Trade} by it's integer id.
     * @param id The id of the trade to find
     * @return {@link com.citi.training.trades.model.Trade} that was found or HTTP 404
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Trade findById(@PathVariable int id) {
        LOG.debug("findById() was called, id: " + id);
        return tradesService.findById(id);
    }
    
    /**
     * Create a new {@link com.citi.training.trades.model.Trade} to save to the DAO
     * @param trade object to be added
     * @return an HttpEntity with an {@link com.citi.training.trades.model.Trade} object and HTTP Status 201 or 404
     */
    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Trade> create(@RequestBody Trade trade) {
        LOG.debug("create was called, Trade: " + trade);
        return new ResponseEntity<Trade>(tradesService.create(trade),
                                            HttpStatus.CREATED);
    }
    
    /**
     * Delete an Employee
     * @param id The id of the employee to find
     */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
        LOG.debug("deleteById was called, id: " + id);
        tradesService.deleteById(id);
    }
}
