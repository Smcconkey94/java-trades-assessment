package com.citi.training.trades.model;

public class Trade {

	private int id;
	private double price;
	private String stock;
	private int volume;
	
	
	public Trade() {}

	public Trade(int id, double price, String stock, int volume){
		this.id = id;
		this.price = price;
		this.stock = stock;
		this.volume = volume;
	}
	
	 public Trade(double price, String stock, int volume) {
	        this(-1, price, stock, volume);
	    }
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}
	
	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	@Override
	public String toString() {
		return "Trade [id=" + id + ", price=" + price + ", stock=" + stock + ", volume=" + volume + "]";
	}

}