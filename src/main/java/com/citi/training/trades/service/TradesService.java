package com.citi.training.trades.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.TradesDao;
import com.citi.training.trades.model.Trade;

@Component
public class TradesService {
	@Autowired
	private TradesDao tradesDao;
	
	public List<Trade> findAll(){
		return tradesDao.findAll();
	}

    public Trade findById(int id) {
		return tradesDao.findById(id);

    };

    public Trade create(Trade trade) {
    	return tradesDao.create(trade);
    };

    public void deleteById(int id) {
    	tradesDao.deleteById(id);
    };
}
